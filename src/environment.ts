export const environment = {
    /** User name for the HomeKit accessory */
    username: process.env.USER ?? 'AB:CD:52:F8:A6:9B',
    /** HomeKit pin code for the accessory */
    pincode: process.env.PINCODE ?? '466-36-486',
    /** Port in which to run the HAP server */
    port: Number(process.env.PORT ?? 47129),
    /** GPIO Pin to use for reading the sensor state */
    pin: Number(process.env.PIN ?? 7),
}
