import rpio, { PULL_UP, PULL_DOWN } from "rpio"

export const enum SensorState {
	Open = 0,
	Closed = 1,
}

export class ContactSensor {
	/** Gets the state of the sensor switch
	 *
	 * `1` - Open
	 * `0` - Closed
	 */
	get state(): SensorState {
		return rpio.read(this.pin)
	}

	constructor(
		public pin: number = 7,
		public resistorType: typeof PULL_UP | typeof PULL_DOWN = PULL_UP,
		public pollDuration = 500
	) {
		rpio.open(pin, rpio.INPUT, resistorType)
	}

	/** Starts polling the pin */
	poll(cb: Function) {
		let lastState = this.state
		setInterval(() => {
			const newState = this.state
			if (newState !== lastState) {
				lastState = newState
				cb(newState)
			}
		}, this.pollDuration)
	}
}
