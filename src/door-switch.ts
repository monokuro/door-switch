import {
    Accessory,
    Categories,
    Characteristic,
    CharacteristicEventTypes,
    CharacteristicGetCallback,
    Service,
    uuid,
} from 'hap-nodejs'
import { ContactSensor, SensorState } from './contact-sensor'
import { environment as env } from './environment'

// Door switch/Contact sensor setup
const doorSwitch = new ContactSensor(env.pin)
const doorSwitchService = new Service.ContactSensor('Door Contact Sensor')

// Accessory characteristic events
const doorStateCharacteristic = doorSwitchService.getCharacteristic(
    Characteristic.ContactSensorState
)!

doorStateCharacteristic.on(
    CharacteristicEventTypes.GET,
    (callback: CharacteristicGetCallback) => {
        callback(undefined, doorSwitch.state)
    }
)

doorStateCharacteristic.on(CharacteristicEventTypes.SUBSCRIBE, () => {
    console.log('Door switch state subscribed')
})

// HAP accessory setup
const accessoryUuid = uuid.generate('hap.edwin.door')
const accessory = new Accessory('Door Sensor', accessoryUuid)
accessory.addService(doorSwitchService)

// Accessory information
accessory
    .getService(Service.AccessoryInformation)!
    .setCharacteristic(Characteristic.Manufacturer, 'monokuro')
    .setCharacteristic(Characteristic.Model, 'Pi Zero NodeJS')
    .setCharacteristic(Characteristic.SerialNumber, 'R45PB33RY-P1-0')

/** Starts the HAP server making the accessory discoverable */
export function start() {
    // Expose the accessory
    accessory.publish({
        username: env.username,
        pincode: env.pincode,
        port: env.port,
        category: Categories.DOOR, // value here defines the symbol shown in the pairing screen
    })
    console.log(`
---  Accessory setup  ---
 Running on port: ${env.port}
 Pincode: ${env.pincode}
-------------------------
`)

    // Poll sensor for changes
    doorSwitch.poll((changedState: SensorState) => {
        doorStateCharacteristic.setValue(changedState)
    })
}

if (require.main === module) start()
