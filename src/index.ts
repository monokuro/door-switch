export { ContactSensor, SensorState } from "./contact-sensor"
import { start } from "./door-switch"

if (require.main === module) start()
