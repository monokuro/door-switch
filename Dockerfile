FROM arm32v6/node:15-alpine as builder
WORKDIR /build

# Required to install 'rpio'
RUN apk add -U --no-cache python3 build-base

# npm package setup
RUN npm i -g npm tsc
COPY ./tsconfig.json ./
COPY ./package.json ./package-lock.json ./
RUN npm install

# Copy src files to build
COPY ./src/ src/

# Build
RUN npm run build

# Remove dev dependecies for final container
RUN npm prune --production

###########################################
FROM arm32v6/node:15-alpine
WORKDIR /app

# Prevent showing update message
RUN npm config set update-notifier false

# Setup built app
COPY --from=builder /build/lib ./lib
COPY --from=builder /build/node_modules ./node_modules
COPY ./package.json .

# Start
CMD ["npm", "start"]
