# Door Switch

## NodeJS HomeKit compatible contact sensor

Uses the [HAP-NodeJS](https://github.com/homebridge/HAP-NodeJS) package and [rpio](https://github.com/jperkin/node-rpio) package to setup a HomeKit door switch (contact sensor) to run on a Raspberry Pi Zero with a reed switch.

---

## Running

Package can either be run by using Docker (recommended) or by cloning the repo and building (with: `npm run build`)

### Docker

Image is currently available with the name: `registry.gitlab.com/monokuro/door-switch:latest`

The project can be started quickly by using `docker-compose`:

```bash
docker-compose up -d
```

Or with the docker CLI:

```bash
docker run -d --net=host \
    -v <host persisting data path/volume>:/app/persist \
    --privileged \
    --restart unless-stopped \
    registry.gitlab.com/monokuro/door-switch:latest
```

Make sure to provide a path for persisting data to ensure the accessory maintains state between container or system restarts.
Privileged mode is required for GPIO access.

Accessory details such as the HomeKit pin code, port or GPIO pin to use for reading the sensor can be configured with environment variables (see [docker-compose.yaml](https://gitlab.com/monokuro/door-switch/-/blob/master/docker-compose.yaml) for supported variables)
